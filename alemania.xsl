<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <head>
                <title>
                    <xsl:value-of select="pais/nombre"/>
                </title>
                <link href="estilos1.css" rel="stylesheet" type="text/css"/>
            </head>
            <body>
                <h1>
                    <xsl:value-of select="pais/nombre"/>
                </h1>
                <p class="imagen">
                    <img alt="Bandera de Alemania" src="img/bandera_de_alemania.png"/>
                </p>
                <xsl:apply-templates select="pais"/>

                <h3>Fotografías</h3>

                <xsl:apply-templates select="pais/fotografiadelpais"/>

            </body>
        </html>

    </xsl:template>
    <xsl:template match="pais">
        <dl>
            <dt>Nombre</dt>
            <dd>
                <xsl:value-of select="nombre"/>
            </dd>
        </dl>
        <dl>
            <dt>Población</dt>
            <dd><xsl:value-of select="poblacion"/>habitantes
            </dd>
        </dl>
        <dl>
            <dt>Continente</dt>
            <dd>
                <xsl:value-of select="continente"/>
            </dd>
        </dl>
        <dl>
            <dt>Nombre capital</dt>
            <dd>
                <xsl:value-of select="nombrecapital"/>
            </dd>
        </dl>
        <dl>
            <dt>Coordenadas</dt>
            <dd>Latitud:
                <xsl:value-of select="coordenadasgpsdelacapital/latitud"/> | Longitud:<xsl:value-of select="coordenadasgpsdelacapital/longitud"/>
            </dd>
        </dl>
        <dl>
            <dt>Tiene costa</dt>
            <dd><xsl:value-of select="tienecosta"/></dd>
        </dl>



    </xsl:template>
    <xsl:template match="pais/fotografiadelpais">

        <h4>
            <xsl:value-of select="nombre"/>
        </h4>
        <img>
            <xsl:attribute name="src">
                <xsl:value-of select="url"/>
            </xsl:attribute>
        </img>
        <p>
            <xsl:value-of select="descripcion"/>
        </p>


    </xsl:template>
</xsl:stylesheet>
